#include "../include/Defines.h"
#include "../include/FileArchivist.h"
#include "../include/FileRecorder.h"
#include "../include/Logger.h"
#include "../include/PrivateFileRecorder.h"
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>
namespace X
{
class TestRecorder : public LogRecorder
{
public:
	TestRecorder( const std::string& name )
		: LogRecorder( name )
		, _testValue( "" )
	{
	}
	// LogRecorder interface
	void save( LOG_TYPE type, const std::string& msg )
	{
		_testValue = msg;
	}
	std::string _testValue;
};
class TestArchivist : public Archivist
{
public:
	TestArchivist()
		: Archivist( ARCHIVE_DATA_MODE::ARCHIVE )
		, _testValue( false )
	{
	}
	// Archivist interface
	std::string archive() override
	{
		_testValue = true;
		return "executed";
	}
	bool _testValue;
};

TEST( logger, useExample )
{
	auto recorder  = std::make_shared< FileRecorder >( "file" );
	auto archivist = std::make_shared< FileArchivist >( X::ARCHIVE_DATA_MODE::ARCHIVE );
	Logger::instance().addRecorder( recorder );
	Logger::instance().addArchivist( "file", archivist );
}
TEST( logger, addRecorder )
{
	Logger l;
	auto r1 = std::make_shared< TestRecorder >( "r1" );
	l.addRecorder( r1 );
	EXPECT_EQ( l._recorders.size(), 1 ) << "invalid recorder count";
	EXPECT_EQ( l._recorders[0], r1 ) << "invalid recorders content";
}
TEST( logger, addRecorder2 )
{
	Logger l;
	auto r1 = std::make_shared< TestRecorder >( "r1" );
	auto t1 = std::make_shared< TestArchivist >();
	l.addRecorder( r1, t1 );
	EXPECT_EQ( l._recorders.size(), 1 ) << "invalid recorder count";
	EXPECT_EQ( l._recorders[0], r1 ) << "invalid recorders content";
	EXPECT_TRUE( t1->_testValue ) << "archive function not executed";
}
TEST( logger, addArchivist )
{
	Logger l;
	auto r1 = std::make_shared< TestRecorder >( "r1" );
	auto t1 = std::make_shared< TestArchivist >();
	l.addRecorder( r1 );
	EXPECT_EQ( l._recorders.size(), 1 ) << "invalid recorder count";
	EXPECT_EQ( l._recorders[0], r1 ) << "invalid recorders content";
	l.addArchivist( "r1", t1 );
	EXPECT_TRUE( t1->_testValue ) << "archive function not executed";
}
TEST( logger, removeRecorder )
{
	Logger l;
	auto r1 = std::make_shared< TestRecorder >( "r1" );
	l.addRecorder( r1 );
	EXPECT_EQ( l._recorders.size(), 1 ) << "invalid recorder count after add";
	EXPECT_EQ( l._recorders[0], r1 ) << "invalid recorders content";
	l.removeRecorder( "r1" );
	EXPECT_EQ( l._recorders.size(), 0 ) << "invalid recorder count after remove";
}
TEST( logger, recorder )
{
	Logger l;
	auto r1 = std::make_shared< TestRecorder >( "r1" );
	auto r2 = std::make_shared< TestRecorder >( "r2" );
	auto r3 = std::make_shared< TestRecorder >( "r3" );
	l.addRecorder( r1 );
	l.addRecorder( r2 );
	l.addRecorder( r3 );
	EXPECT_EQ( r1, l.recorder( "r1" ) );
	EXPECT_EQ( r2, l.recorder( "r2" ) );
	EXPECT_EQ( r3, l.recorder( "r3" ) );
}
TEST( logger, recorderNames )
{
	std::vector< std::string > reference{ "r1", "r2", "r3" };
	Logger l;
	auto r1 = std::make_shared< TestRecorder >( "r1" );
	auto r2 = std::make_shared< TestRecorder >( "r2" );
	auto r3 = std::make_shared< TestRecorder >( "r3" );
	l.addRecorder( r1 );
	l.addRecorder( r2 );
	l.addRecorder( r3 );
	EXPECT_EQ( reference, l.recordersNames() ) << "invalid recorders names";
}
TEST( logger, toString )
{
	std::string referenceValue{ "red fox jump" };
	std::string referenceInt{ "12345" };
	std::string referenceDouble{ "3.1415" };
	//std string
	EXPECT_EQ( X::Logger::instance().toString( referenceValue ), referenceValue ) << "invalid cast std string";
	//char
	const char* charValue = "red fox jump";
	EXPECT_EQ( X::Logger::instance().toString( charValue ), referenceValue ) << "invalid cast char";
	//int
	int intValue = 12345;
	EXPECT_EQ( X::Logger::instance().toString( intValue ), referenceInt ) << "invalid cast int";
	//double
	double doubleValue = 3.1415;
	EXPECT_EQ( X::Logger::instance().toString( doubleValue ), referenceDouble ) << "invalid cast double";
	//	QString QDateTime
}
TEST( logger, write )
{
	Logger l;
	auto r1 = std::make_shared< TestRecorder >( "r1" );
	l.addRecorder( r1 );
	l.write< bool >( LOG_TYPE::DEBUG, true );
	EXPECT_EQ( r1->_testValue, "1" );
}
} // namespace X
