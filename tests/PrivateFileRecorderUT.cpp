#include "../include/PrivateFileRecorder.h"
#include <fstream>
#include <gtest/gtest.h>
#include <regex>
namespace X
{
TEST( privateFileRecorder, save )
{
	// create file
	std::string fileName( "test_file.txt" );
	std::fstream outputFile;
	outputFile.open( fileName, std::ios_base::out | std::ios::trunc );
	EXPECT_TRUE( outputFile.is_open() ) << "can't open output file";
	outputFile.close();

	PrivateFileRecorder recorder;
	recorder.setEnableConsoleColorsTags( false );
	recorder.setLogPath( fileName );
	std::vector< std::pair< LOG_TYPE, std::string > > reference{ { LOG_TYPE::DEBUG, "debug log " },
																 { LOG_TYPE::INFORMATION, "information log " },
																 { LOG_TYPE::WARNING, "warning log " },
																 { LOG_TYPE::ERROR, "error log " } };
	for ( auto record : reference )
	{
		recorder.save( record.first, record.second );
	}
	std::ifstream inputFile;
	inputFile.open( fileName, std::ios_base::in );
	EXPECT_TRUE( inputFile.is_open() ) << "cant open result file";
	std::string line;
	std::regex rg( "" );
	int i = 0;
	while ( std::getline( inputFile, line ) )
	{
		EXPECT_TRUE( i < reference.size() ) << "invalid file size";
		rg.assign( ".*" + reference[i].second );
		std::cout << line << "\t" << reference[i].second << "\n";
		EXPECT_TRUE( std::regex_match( line, rg ) ) << "invalid saved value";
		++i;
	}
	EXPECT_FALSE( i == 0 ) << "nothing was saved";
}
TEST( privateFileRecorder, queueSave )
{
	// create file
	std::string fileName( "test_file.txt" );
	std::fstream outputFile;
	outputFile.open( fileName, std::ios_base::out | std::ios::trunc );
	EXPECT_TRUE( outputFile.is_open() ) << "can't open output file";
	outputFile.close();
	PrivateFileRecorder recorder( "test_recorder" );
	recorder.setLogPath( fileName );
	std::vector< std::string > reference{ "log 1", "log 2", "log 3" };
	for ( auto value : reference )
	{
		recorder._messages.push( { LOG_TYPE::DEBUG, value } );
	}
	EXPECT_EQ( reference.size(), recorder._messages.size() );
	recorder.queuedSave();
	std::ifstream inputFile;
	inputFile.open( fileName, std::ios_base::in );
	EXPECT_TRUE( inputFile.is_open() ) << "cant open result file";
	std::string line;
	std::regex rg( "" );
	int i = 0;
	while ( std::getline( inputFile, line ) )
	{
		EXPECT_TRUE( i < reference.size() ) << "invalid file size";
		rg.assign( ".*" + reference[i] );
		EXPECT_TRUE( std::regex_match( line, rg ) ) << "invalid saved value";
		++i;
	}
	EXPECT_FALSE( i == 0 ) << "nothing was saved";
}
TEST( privateFileRecorder, pushMessage )
{
	EXPECT_TRUE( false );
}
TEST( privateFileRecorder, setEnableConsoleColorsTags )
{
	PrivateFileRecorder recorder;
	recorder.setEnableConsoleColorsTags( std::string( "YES" ) );
	EXPECT_EQ( recorder._enableConsoleColorsTags, true ) << "invalid value after set yes";
	recorder.setEnableConsoleColorsTags( std::string( "NO" ) );
	EXPECT_EQ( recorder._enableConsoleColorsTags, false ) << "invalid value after set no";
	recorder.setEnableConsoleColorsTags( std::string( "ENABLE_CONSOLE_COLORS_TAGS: YES" ) );
	EXPECT_EQ( recorder._enableConsoleColorsTags, true ) << "invalid value after set yes";
	recorder.setEnableConsoleColorsTags( std::string( "ENABLE_CONSOLE_COLORS_TAGS=NO" ) );
	EXPECT_EQ( recorder._enableConsoleColorsTags, false ) << "invalid value after set no";
}
TEST( privateFileRecorder, trim )
{
	PrivateFileRecorder recorder;
	std::string str = "red fox";
	EXPECT_EQ( str, recorder.trim( " " + str + " " ) );
	EXPECT_EQ( str, recorder.trim( "\t" + str + "\n\t" ) );
	EXPECT_EQ( str, recorder.trim( "\n\t " + str + "\n " ) );
	EXPECT_FALSE( str == recorder.trim( str + "2 " ) );
	EXPECT_FALSE( str == recorder.trim( "2" + str ) );
}
} // namespace X
