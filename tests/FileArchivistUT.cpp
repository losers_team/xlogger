#include "../XLZ4/include/CLZ4.h"
#include "../include/FileArchivist.h"
#include <ctime>
#include <filesystem>
#include <fstream>
#include <gtest/gtest.h>
#include <iostream>
namespace X
{

TEST( fileArchivist, archive )
{
	// create file
	std::string fileName( "test_file.txt" );
	std::fstream outputFile;
	outputFile.open( fileName, std::ios_base::out );
	EXPECT_TRUE( outputFile.is_open() ) << "can't open output file";
	outputFile.close();

	FileArchivist archivist( ARCHIVE_DATA_MODE::ARCHIVE );
	archivist.setOldDataPath( fileName );
	std::string path = archivist.generateNewName( fileName );
	EXPECT_EQ( path, archivist.archive() );
	path = fileName + ".1";
	archivist.setMode( ARCHIVE_DATA_MODE::TRUNCATE );
	EXPECT_EQ( path, archivist.archive() );
}
TEST( fileArchivist, generateNewName )
{
	std::time_t time( 1568568358 );
	std::string reference( "Sun_Sep_15_19:25:58_2019_name.lz4" );
	std::string name( "name" );
	FileArchivist archivist( ARCHIVE_DATA_MODE::ARCHIVE );
	EXPECT_EQ( archivist.generateNewName( name, time ), reference );
}
TEST( fileArchivist, compressDecompressFile )
{
	// create reference content
	//	const std::vector< std::string > reference{ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
	const std::vector< int > reference{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	// create file
	std::string name( "test_file.txt" );
	std::fstream outputFile;
	outputFile.open( name, std::ios_base::out );
	// add content
	EXPECT_TRUE( outputFile.is_open() ) << "can't open output file";
	for ( auto i : reference )
	{
		outputFile << i << "\n";
	}
	outputFile.close();
	// compress
	FileArchivist archivist( ARCHIVE_DATA_MODE::ARCHIVE );
	std::string compressedFileName = archivist.compressFile( name );
	// decopress
	std::string decompressedFileName = archivist.decompressFile( compressedFileName );
	// compare to reference
	std::fstream inputFile;
	inputFile.open( decompressedFileName, std::ios_base::in );
	EXPECT_TRUE( inputFile.is_open() );
	std::string line;
	int i = 0;
	while ( std::getline( inputFile, line ) )
	{
		EXPECT_EQ( std::atoi( line.c_str() ), reference[i] ) << "invalid decompress result";
		++i;
	}
}
TEST( fileArchivist, renameFile )
{
	//use smoe uniqe name
	auto t           = std::time( nullptr );
	auto tm          = *std::localtime( &t );
	std::size_t size = 32;
	char name[size];
	size = std::strftime( name, size, "%Y-%m-%d_%H-%M-%S", &tm );

	//check if file exists, it shoud not exists
	std::fstream file( name );
	EXPECT_FALSE( file.good() );

	//create this file
	file.open( name, std::ios::out );
	EXPECT_TRUE( file.good() );
	file.close();

	//use rename function
	FileArchivist archivist( ARCHIVE_DATA_MODE::ARCHIVE );
	auto newName = archivist.renameFile( std::string( name, size ) );
	EXPECT_TRUE( newName != "" );

	//check new file exists now it shoud exies
	std::fstream file2( newName.c_str() );
	EXPECT_TRUE( file2.good() );
}
} // namespace X
