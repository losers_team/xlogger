#include "../include/Archivist.h"
#include <gtest/gtest.h>
namespace X
{
class TestArchivist : public Archivist
{
public:
	TestArchivist()
		: Archivist( ARCHIVE_DATA_MODE::ARCHIVE )
		, _testValue( false )
	{
	}
	// Archivist interface
	std::string archive() override
	{
		_testValue = true;
		return "executed";
	}
	bool _testValue;
};

TEST( archivist, setMode )
{
	TestArchivist arch;
	arch.setMode( "ARCHIVE_DATA_MODE: ARCHIVE" );
	EXPECT_EQ( arch._mode, ARCHIVE_DATA_MODE::ARCHIVE );
	arch.setMode( "ARCHIVE_DATA_MODE: TRUNCATE" );
	EXPECT_EQ( arch._mode, ARCHIVE_DATA_MODE::TRUNCATE );
	arch.setMode( "ARCHIVE" );
	EXPECT_EQ( arch._mode, ARCHIVE_DATA_MODE::ARCHIVE );
	arch.setMode( "TRUNCATE" );
	EXPECT_EQ( arch._mode, ARCHIVE_DATA_MODE::TRUNCATE );
}
TEST( archivist, archiveDataModeToString )
{
	TestArchivist arch;
	EXPECT_EQ( arch.archiveDataModeToString( ARCHIVE_DATA_MODE::ARCHIVE ), "ARCHIVE" );
	EXPECT_EQ( arch.archiveDataModeToString( ARCHIVE_DATA_MODE::TRUNCATE ), "TRUNCATE" );
}
TEST( archivist, archiveDataModeFromString )
{
	TestArchivist arch;
	EXPECT_EQ( arch.archiveDataModeFromString( "ARCHIVE" ), ARCHIVE_DATA_MODE::ARCHIVE );
	EXPECT_EQ( arch.archiveDataModeFromString( "TRUNCATE" ), ARCHIVE_DATA_MODE::TRUNCATE );
}
} // namespace X
