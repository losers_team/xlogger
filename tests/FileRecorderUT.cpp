#include "../include/FileRecorder.h"
#include <cstdio>
#include <fstream>
#include <gtest/gtest.h>
#include <regex>
#include <thread>
namespace X
{
TEST( fileRecorder, save )
{
	//create reference value
	std::vector< std::string > reference{ "message0", "message1", "message2", "message3" };
	//delete test file if it exists
	std::string logPath = "write_test_file.log";
	std::ifstream file( logPath, std::ios_base::in );
	if ( file.good() )
	{
		file.close();
		EXPECT_EQ( remove( logPath.c_str() ), 0 ) << "error to remove test file";
	}
	//save logs
	FileRecorder recorder;
	recorder.setLogPath( logPath );
	for ( auto line : reference )
	{
		recorder.save( LOG_TYPE::DEBUG, line );
	}
	//wait until all data will be saved
	std::this_thread::sleep_for( std::chrono::milliseconds( 100 ) );
	//read logs
	std::ifstream result;
	result.open( logPath );
	EXPECT_TRUE( result.is_open() ) << " output file not open ";
	//compare valuel
	std::string line;
	std::vector< std::string > output;
	while ( std::getline( result, line ) )
	{
		output.push_back( line );
	}
	EXPECT_EQ( reference.size(), output.size() ) << "invalid output vector size";
	std::regex rg( "" );
	for ( int i = 0; i < reference.size(); ++i )
	{

		rg.assign( ".*" + reference[i] );
		EXPECT_TRUE( std::regex_match( output[i], rg ) )
			<< "invalid output value reference value " << reference[i] << " output value " << output[i];
	}
}
} // namespace X
