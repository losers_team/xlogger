#include "Logger.h"
#include "LogRecorderManager.h"
#include <algorithm>
#include <iostream>
X::Logger& X::Logger::instance()
{
	static Logger out;
	return out;
}

X::Logger::Logger()
	: _type( X::LOG_TYPE::INFORMATION )
{
}

X::Logger::~Logger() {}

void X::Logger::addRecorder( std::shared_ptr< X::LogRecorder > recorder )
{
	removeRecorder( recorder->name() );
	_recorders.push_back( recorder );
}
void X::Logger::addRecorder( std::shared_ptr< X::LogRecorder > recorder, std::shared_ptr< Archivist > archivist )
{
	LogRecorderManager manager;
	manager.manage( recorder, archivist );
	addRecorder( recorder );
}

void X::Logger::addArchivist( const std::string& recorderName, std::shared_ptr< X::Archivist > archivist )
{
	auto r = recorder( recorderName );
	if ( r )
	{
		LogRecorderManager manager;
		manager.manage( r, archivist );
	}
}

void X::Logger::removeRecorder( const std::string& name )
{
	for ( auto i = _recorders.begin(); i != _recorders.end(); ++i )
	{
		if ( ( *i )->name() == name )
		{
			//it's only one recorder with that name, recorders orders is not important
			std::swap( *i, _recorders.back() );
			_recorders.pop_back();
			break;
		}
	}
}

std::shared_ptr< X::LogRecorder > X::Logger::recorder( const std::string& name )
{
	std::shared_ptr< X::LogRecorder > out;
	auto iterator = std::find_if( _recorders.cbegin(), _recorders.cend(), [name]( std::shared_ptr< X::LogRecorder > recorder ) {
		return ( recorder->name() == name );
	} );
	if ( iterator != _recorders.cend() )
	{
		out = *iterator;
	}
	return out;
}

std::vector< std::string > X::Logger::recordersNames()
{
	std::vector< std::string > out;
	std::transform( _recorders.cbegin(), _recorders.cend(), std::back_inserter( out ), []( std::shared_ptr< X::LogRecorder > r ) {
		return r->name();
	} );
	return out;
}
