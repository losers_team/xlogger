#include "PrivateFileRecorder.h"
#include "../XFileSettingsLoader/include/FileSettingsLoader.h"
#include <ctime>
#include <fstream>
#include <future>
#include <iostream>
#include <lz4.h>
#include <regex>

X::PrivateFileRecorder::PrivateFileRecorder( const std::string& name )
	: X::LogRecorder( name )
	, _enableConsoleColorsTags( false )
{
}

void X::PrivateFileRecorder::save( X::LOG_TYPE type, const std::string& msg )
{
	std::fstream out;
	out.open( _logPath, std::ios_base::out | std::ios_base::app );
	if ( out.is_open() )
	{
		std::time_t t    = std::time( nullptr );
		std::string time = trim( std::ctime( &t ) );
		if ( _enableConsoleColorsTags )
		{
			out << time << " " << _colorsTags.at( type ) << msg << _colorSufix << "\n";
		}
		else
		{
			out << time << " " << msg << "\n";
		}
		out.close();
	}
}

void X::PrivateFileRecorder::queuedSave()
{
	while ( !_messages.empty() )
	{
		_queueMutex.lock();
		std::pair< X::LOG_TYPE, std::string > msg = _messages.front();
		_messages.pop();
		_queueMutex.unlock();
		save( msg.first, msg.second );
	}
	_isRunning = false;
}

void X::PrivateFileRecorder::pushMessage( X::LOG_TYPE type, const std::string& msg )
{
	_queueMutex.lock();
	_messages.push( { type, msg } );
	_queueMutex.unlock();
	if ( !_isRunning )
	{
		_isRunning = true;
		std::thread( &X::PrivateFileRecorder::queuedSave, this ).detach();
	}
}

bool X::PrivateFileRecorder::enableConsoleColorsTags() const
{
	return _enableConsoleColorsTags;
}

void X::PrivateFileRecorder::setEnableConsoleColorsTags( bool enableConsoleColorsTags )
{
	_enableConsoleColorsTags = enableConsoleColorsTags;
}

void X::PrivateFileRecorder::setEnableConsoleColorsTags( const std::string& value )
{
	std::regex regexp( R"(.*\bNO\b)" );
	if ( std::regex_match( value, regexp ) )
	{
		_enableConsoleColorsTags = false;
	}
	else
	{
		regexp = R"(.*\bYES\b)";
		if ( std::regex_match( value, regexp ) )
		{
			_enableConsoleColorsTags = true;
		}
	}
}

std::string X::PrivateFileRecorder::logPath() const
{
	return _logPath;
}

void X::PrivateFileRecorder::setLogPath( const std::string& logPath )
{
	_logPath = logPath;
}

bool X::PrivateFileRecorder::readConfigFile()
{
	FileSettingsLoader loader( "xlogger.conf" );
	loader.addProperties< PrivateFileRecorder >( "LOG_PATH", this, &PrivateFileRecorder::setLogPath );
	loader.addProperties< PrivateFileRecorder >( "ENABLE_CONSOLE_COLORS_TAGS", this, &PrivateFileRecorder::setEnableConsoleColorsTags );
	return loader.load();
}

std::string X::PrivateFileRecorder::trim( std::string value )
{
	std::string chars = "\t\n\v\f\r ";
	std::string out;
	//rtrim
	out = value.erase( 0, value.find_first_not_of( chars ) );
	//ltrim
	out = out.erase( out.find_last_not_of( chars ) + 1 );
	return out;
}
