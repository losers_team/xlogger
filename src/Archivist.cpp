#include "Archivist.h"

X::Archivist::Archivist(X::ARCHIVE_DATA_MODE mode):
	_mode(mode)
{

}
X::ARCHIVE_DATA_MODE X::Archivist::mode() const
{
	return _mode;
}

void X::Archivist::setMode(const ARCHIVE_DATA_MODE &mode)
{
	_mode = mode;
}
void X::Archivist::setMode(const std::string &str)
{
	std::regex value;
	for( auto mode : ARCHIVE_DATA_MODE() )
	{
		value.assign( ".*\\b" + archiveDataModeToString (mode) +"\\b");
		if( std::regex_match(str, value) )
		{
			_mode = mode;
			break;
		}
	}
}
std::string X::Archivist::archiveDataModeToString(X::ARCHIVE_DATA_MODE value)
{
	std::string out;
	if( value == ARCHIVE_DATA_MODE::ARCHIVE )
	{
		out = "ARCHIVE";
	}
	else if( value == ARCHIVE_DATA_MODE::TRUNCATE )
	{
		out = "TRUNCATE";
	}
	return out;
}
X::ARCHIVE_DATA_MODE X::Archivist::archiveDataModeFromString(const std::string &value)
{
	ARCHIVE_DATA_MODE out;
	if( value == "ARCHIVE" )
	{
		out = ARCHIVE_DATA_MODE::ARCHIVE;
	}
	else if( value == "TRUNCATE" )
	{
		out = ARCHIVE_DATA_MODE::TRUNCATE;
	}
	return out;
}
