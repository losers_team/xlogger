#include "FileArchivist.h"
#include "CLZ4.h"
#include <iostream>
X::FileArchivist::FileArchivist( X::ARCHIVE_DATA_MODE mode )
	: Archivist( mode )
{
}

std::string X::FileArchivist::archive()
{
	std::string path;
	if ( _mode == ARCHIVE_DATA_MODE::ARCHIVE )
	{
		path = compressFile( _oldDataPath );
	}
	else
	{
		path = renameFile( _oldDataPath );
	}
	return path;
}

std::string X::FileArchivist::oldDataPath() const
{
	return _oldDataPath;
}

void X::FileArchivist::setOldDataPath( const std::string& oldDataPath )
{
	_oldDataPath = oldDataPath;
}
std::string X::FileArchivist::compressFile( const std::string& oldPath )
{
	std::string newPath = generateNewName( oldPath );
	bool out            = X::CLZ4::compressFile( oldPath, newPath );
	if ( !out )
	{
		newPath.clear();
	}
	return newPath;
}
std::string X::FileArchivist::decompressFile( const std::string& oldPath )
{
	std::string newPath = generateNewName( oldPath );
	bool out            = X::CLZ4::decompressFile( oldPath, newPath );
	if ( !out )
	{
		newPath.clear();
	}
	return newPath;
}

std::string X::FileArchivist::renameFile( const std::string& fileName )
{
	std::string newName = fileName + ".1";
	std::rename( fileName.c_str(), newName.c_str() );
	return newName;
}
std::string X::FileArchivist::generateNewName( const std::string& fileName, const time_t& time )
{
	std::string out;
	std::string pattern = R"(.*lz4)";
	try
	{
		std::regex r2( pattern );
	}
	catch ( const std::regex_error& ex )
	{
		std::cerr << ex.what() << ex.code();
	}

	if ( std::regex_search( fileName, std::regex( ".*lz4" ) ) )
	{
		out = std::regex_replace( fileName, std::regex( ".lz4" ), "" );
	}
	else
	{
		std::string prefix = std::ctime( &time );
		prefix             = std::regex_replace( prefix, std::regex( " |\\n" ), "_" );
		std::string sufix( ".lz4" );
		out = ( prefix + fileName + sufix );
	}
	return out;
}
