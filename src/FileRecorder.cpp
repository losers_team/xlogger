#include "FileRecorder.h"
#include "PrivateFileRecorder.h"

X::FileRecorder::FileRecorder( const std::string& name )
	: X::LogRecorder( name )
	, _p( std::make_shared< X::PrivateFileRecorder >( name ) )

{
}

void X::FileRecorder::save( X::LOG_TYPE type, const std::string& msg )
{
	_p->pushMessage( type, msg );
}

std::string X::FileRecorder::logPath() const
{
	return _p->logPath();
}

void X::FileRecorder::setLogPath( const std::string& logPath )
{
	_p->setLogPath( logPath );
}

bool X::FileRecorder::enableConsoleColorsTags() const
{
	return _p->enableConsoleColorsTags();
}

void X::FileRecorder::setEnableConsoleColorsTags( bool enableConsoleColorsTags )
{
	_p->setEnableConsoleColorsTags( enableConsoleColorsTags );
}

void X::FileRecorder::setEnableConsoleColorsTags( const std::string& value )
{
	_p->setEnableConsoleColorsTags( value );
}
