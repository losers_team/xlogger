#ifndef PRIVATEFILERECORDER_H
#define PRIVATEFILERECORDER_H
#include "LogRecorder.h"
#include <atomic>
#include <filesystem>
#include <gtest/gtest_prod.h>
#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <vector>
namespace X
{
/**
 * @brief The PrivateFileRecorder class. Class is use as pointer to implementation in plmpl technique. It remove implementation details from FileRecorder. It also allow to move more havy operations to other thread.
 */
class PrivateFileRecorder : public LogRecorder
{
	FRIEND_TEST( privateFileRecorder, save );
	FRIEND_TEST( privateFileRecorder, queueSave );
	FRIEND_TEST( privateFileRecorder, pushMessage );
	FRIEND_TEST( privateFileRecorder, setEnableConsoleColorsTags );
	FRIEND_TEST( privateFileRecorder, trim );

public:
	/**
	 * @brief Constructs
	 * @param name
	 */
	explicit PrivateFileRecorder( const std::string& name = "file.log" );
	// LogRecorder interface
	/**
	 * @brief This is ovrrided function to save msg in file.
	 * @param type
	 * @param msg
	*/
	void save( LOG_TYPE type, const std::string& msg ) override;
	/**
	 * @brief Add message to message queue. If saving process is not started, function lunch new thread with saving queue.
	 * @param type
	 * @param msg
	 */
	void pushMessage( LOG_TYPE type, const std::string& msg );
	/**
	 * @brief Getter to member "logPath". Return path to log file.
	 * @return
	 */
	std::string logPath() const;
	/**
	 * @brief Setter to member "logPath". Set path to log file.
	 * @param logPath
	*/
	void setLogPath( const std::string& logPath );

	/**
	 * @brief Getter to member "enableConsoleColorsTags". If this property is enabled, to all log will be added console color tag.
	 * @return
	 */
	bool enableConsoleColorsTags() const;
	/**
	 * @brief Setter to member "enableConsoleColorsTags".
	 * @param enableConsoleColorsTags
	 */
	void setEnableConsoleColorsTags( bool enableConsoleColorsTags );
	/**
	 * @brief Overloaded setter for "enableConsoleColorsTags" member.
	 * @param value
	 */
	void setEnableConsoleColorsTags( const std::string& value );

private:
	std::vector< std::string > _properties{ "LOG_PATH", "ENABLE_CONSOLE_COLORS_TAGS" };
	const std::map< LOG_TYPE, std::string > _colorsTags{ { LOG_TYPE::DEBUG, "\033[1;33m" },
														 { LOG_TYPE::INFORMATION, "\033[0;34m" },
														 { LOG_TYPE::WARNING, "\033[0;36m" },
														 { LOG_TYPE::ERROR, "\033[1;31m" } };
	std::string _colorSufix{ "\033[0m" };
	std::string _logPath;
	bool _enableConsoleColorsTags{ true };
	std::queue< std::pair< LOG_TYPE, std::string > > _messages;
	std::atomic< bool > _isRunning{ false };
	std::mutex _queueMutex;
	/**
	 * @brief Internal function to read configuration file. All values from readed file will be set to current object.
	 * @return
	 */
	bool readConfigFile();
	/**
	 * @brief Helper function to trim string.
	 * @param value
	 * @return
	 */
	std::string trim( std::string value );
	/**
	 * @brief Function save all messages in message queue.
	 */
	void queuedSave();
};
} // namespace X
#endif // PRIVATEFILERECORDER_H
