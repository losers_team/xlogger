#ifndef FILEARCHIVIST_H
#define FILEARCHIVIST_H
#include "Archivist.h"
#include <filesystem>
#include <gtest/gtest_prod.h>
#include <string>
namespace X
{
/**
 * @brief Class to menage old logs file. Apropirate to options set, archving can procede in one of two modes. First one truncate old logs file( ARCHIVE_DATA_MODE::TRUNCATE ). Second one (ARCHIVE_DATA_MODE::ARCHIVE) use lz4 algorithm to pack and store old logs files.
 */
class FileArchivist : public Archivist
{
	FRIEND_TEST( fileArchivist, archive );
	FRIEND_TEST( fileArchivist, generateNewName );
	FRIEND_TEST( fileArchivist, compressDecompressFile );
	FRIEND_TEST( fileArchivist, renameFile );

public:
	/**
	 * @brief Class constructor. It get only one parameter to setup behaviour in against old log files.
	 * @param mode
	 */
	explicit FileArchivist( ARCHIVE_DATA_MODE mode );
	// Archivist interface
	std::string archive() override;
	/**
	 * @brief Getter to member odlDataPath.
	 * @return
	 */
	std::string oldDataPath() const;
	/**
	 * @brief Setter to member oldDataPath.
	 * @param oldDataPath
	 */
	void setOldDataPath( const std::string& oldDataPath );

private:
	std::string _oldDataPath;
	std::string generateNewName( const std::string& name, const std::time_t& time = std::time( nullptr ) );
	/**
	 * @brief Function compress old data using lz4 algorithm.
	 * @param fileName
	 * @return
	 */
	std::string compressFile( const std::string& fileName );
	/**
	 * @brief Function decompress file using lz4 algorithm.
	 * @param fileName
	 * @return
	 */
	std::string decompressFile( const std::string& fileName );
	/**
	 * @brief Function append sufix to file name.
	 * @param fileName
	 * @return
	 */
	std::string renameFile( const std::string& fileName );
};
} // namespace X
#endif // FILEARCHIVIST_H
