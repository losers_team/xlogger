#ifndef LOGRECORDERMANAGER_H
#define LOGRECORDERMANAGER_H
#include "Archivist.h"
#include "LogRecorder.h"
#include <gtest/gtest.h>
#include <memory>
namespace X
{
/**
 * @brief Main responsability of this class is menage log recorders.
 */
class LogRecorderManager
{
	FRIEND_TEST( logRecorderManager, manage );

public:
	/**
	 * @brief Constructs LogRecordMenager object.
	 */
	LogRecorderManager();
	/**
	 * @brief This function Initialize recorder,  Archive old data.
	 * @param recorder
	 * @param archivist
	 */
	void manage( std::shared_ptr< LogRecorder > recorder, std::shared_ptr< Archivist > archivist );
};
} // namespace X

#endif // LOGRECORDERMANAGER_H
