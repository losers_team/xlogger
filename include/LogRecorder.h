#ifndef LOGRECORDER_H
#define LOGRECORDER_H
#include <string>
#include "Defines.h"

namespace X
{
/**
 * @brief The XLogRecorder class
 * This class is used as interface to define particular recorder. Particular recorder save or push data to target for example file.
 * Save method must be override in derivative class.
 * Recorders can be used in logger class as a observers.
 */
class LogRecorder
{
public:
    /**
	* @brief Constructs Log Recorder. Passed argument is used to initialize class member: "name". "Name" can be used to manage recorder.
    * @param name 
    */
	explicit LogRecorder(const std::string& name );
    virtual ~LogRecorder() = default;
    /**
     * @brief Virtual function to save messages.
	 * @param msg - message to save
	 * @param type - determinate message type, in which this message should be saved
     */
    virtual void save( X::LOG_TYPE type, const std::string& msg ) = 0;
    /**
     * @brief name return name property.
     * @return
     */
    std::string name();
protected:
	/**
	 * @brief _name
	 */
    std::string _name;    
};
}

#endif // LOGRECORDER_H
