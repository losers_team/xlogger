#ifndef LOGGER_H
#define LOGGER_H
#include "Archivist.h"
#include "Defines.h"
#include "LogRecorder.h"
#include <gtest/gtest_prod.h>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#define __trace__ X::Logger::instance().write< std::string >( X::LOG_TYPE::DEBUG, __fun__ );
#define xDebug( x ) X::Logger::instance().write< decltype( x ) >( X::LOG_TYPE::DEBUG, x )
#define xInfo( x ) X::Logger::instance().write< decltype( x ) >( X::LOG_TYPE::INFORMATION, x )
#define xWarning( x ) X::Logger::instance().write< decltype( x ) >( X::LOG_TYPE::WARNING, x )
#define xError( x ) X::Logger::instance().write< decltype( x ) >( X::LOG_TYPE::ERROR, x )

namespace X
{
/**
 * @brief Class to logging.
 * Class allow to add recorder witch it save log. It defines logs type and also logging type.
 *
 */
class Logger
{
	FRIEND_TEST( logger, addRecorder );
	FRIEND_TEST( logger, addRecorder2 );
	FRIEND_TEST( logger, addArchivist );
	FRIEND_TEST( logger, removeRecorder );
	FRIEND_TEST( logger, recorder );
	FRIEND_TEST( logger, recorderNames );
	FRIEND_TEST( logger, toString );
	FRIEND_TEST( logger, write );

public:
	/**
     * @brief instance Singleton implementation
     * @return
     */
	static Logger& instance();
	~Logger();
	/**
     * @brief Method append new log recorder object. 
     * All messages will be pushed to all defined recorders. 
     * Recording order is determining by order of appending recorders.
	 * @param recorder
     */
	void addRecorder( std::shared_ptr< LogRecorder > recorder );
	/**
	 * @brief Overloaded function witch allow also menage old data.
	 * @param recorder
	 * @param archivist
	 */
	void addRecorder( std::shared_ptr< LogRecorder > recorder, std::shared_ptr< Archivist > archivist );
	/**
	 * @brief Function add archivist to recorder. After append recorder old data will be archive.
	 * @param recorderName
	 * @param archivist
	 */
	void addArchivist( const std::string& recorderName, std::shared_ptr< Archivist > archivist );

	/**
     * @brief Method remove recorder, witch name was passing to function as argument.
     * @param name 
     */
	void removeRecorder( const std::string& name );
	/**
     * @brief Get the Recorder object by it name.
     * 
     * @param name 
     * @return std::shared_ptr<LogRecorder> 
     */
	std::shared_ptr< LogRecorder > recorder( const std::string& name );
	/**
     * @brief Get the recorders names.
     * @return std::vector<std::string> 
     */
	std::vector< std::string > recordersNames();
	/**
     * Function try to convert object to string
     */
	template < typename T > static std::string toString( T value )
	{
		std::string out;
		std::ostringstream stream;
		stream << value;
		out = stream.str();
		return out;
	}
	/**
     * @brief Basic method to record particular messseges with it types.
     * 
     * @param type 
     * @param msg 
     */
	template < typename T > void write( X::LOG_TYPE type, T msg )
	{
		auto string = toString< T >( msg );
		for ( const auto& recorder : _recorders )
		{
			recorder->save( type, string );
		}
	}

private:
	X::LOG_TYPE _type;
	std::vector< std::shared_ptr< X::LogRecorder > > _recorders;
	Logger();
};
};     // namespace X
#endif // LOGGER_H
