#ifndef ARCHIVIST_H
#define ARCHIVIST_H
#include "Defines.h"
#include <gtest/gtest.h>
#include <regex>
#include <string>
namespace X
{
/**
 * @brief Interface class to menage old logs.
 */
class Archivist
{
	FRIEND_TEST( archivist, setMode );
	FRIEND_TEST( archivist, archiveDataModeToString );
	FRIEND_TEST( archivist, archiveDataModeFromString );

public:
	/**
	 * @brief Class constructor. It get only one parameter wchich describe behaviour against old log.
	 * @param mode
	 */
	explicit Archivist( ARCHIVE_DATA_MODE mode );
	/**
	 * @brief Function must be implemented in specifict Archivist class.
	 * @return Function return true when all archive process run sucessfully, in other hand it return false.
	 *
	 */
	virtual std::string archive() = 0;
	/**
	 * @brief Getter to member "mode". It return currently set mode towards old log file.
	 * @return
	 */
	ARCHIVE_DATA_MODE mode() const;
	/**
	 * @brief Setter to member "mode". It set mode against old log file.
	 * @param mode
	 */
	void setMode( const ARCHIVE_DATA_MODE& mode );
	/**
	 * @brief Overloaded settter to member "mode".
	 * @param str
	 */
	void setMode( const std::string& str );
	/**
	 * @brief Helper function allow to convert enum ARCHIVE_DATA_MODE to std::string
	 * @param value
	 * @return
	 */
	std::string archiveDataModeToString( ARCHIVE_DATA_MODE value );

	/**
	 * @brief Helper, allow convert std::string to enum ARCHIVE_DATA_mode
	 * @param value
	 * @return
	 */
	ARCHIVE_DATA_MODE archiveDataModeFromString( const std::string& value );

protected:
	/**
	 * @brief Calss member describe behaviour in wchich class menage old logs. There is available two options. First in wchich old data is not now important and can be removed. In second one, old data are needed and it  must be archiving in some way. Way to trucate and archiving must be implemented in subclass.
	 */
	ARCHIVE_DATA_MODE _mode;
	/**
	 * @brief List of members which can be serialized.
	 */
	std::vector< std::string > _properties{ "ARCHIVE_DATA_MODE" };
};
} // namespace X

#endif // ARCHIVIST_H
