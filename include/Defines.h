#ifndef DEFINES_H
#define DEFINES_H
#include <type_traits>
namespace X
{
enum class LOG_TYPE
{
    DEBUG,
    INFORMATION,
    WARNING,
    ERROR
};
enum class ARCHIVE_DATA_MODE
{
	ARCHIVE,
	TRUNCATE,
	BEGIN = ARCHIVE,
	END = TRUNCATE
};
/**
 * @brief Return next value of enum class, counting from argument value.
 * @param mode
 * @return
 */
inline ARCHIVE_DATA_MODE operator++(ARCHIVE_DATA_MODE& mode)
{
	return mode = (ARCHIVE_DATA_MODE)(std::underlying_type<ARCHIVE_DATA_MODE>::type(mode)+1);
}
/**
 * @brief Return value passed in argument. This operator is needed to use enum class in range loop.
 * @param mode
 * @return
 */
inline ARCHIVE_DATA_MODE operator*(ARCHIVE_DATA_MODE& mode)
{
	return mode;
}
/**
 * @brief Return first value from enum ARCHIVE_DATA_MODE.
 * @return
 */
inline ARCHIVE_DATA_MODE begin( ARCHIVE_DATA_MODE /*mode*/ )
{
	return ARCHIVE_DATA_MODE::BEGIN;
}
/**
 * @brief Return end value (last one ++) from enum ARCHIVE_DATA_MODE.
 * @param mode
 * @return
 */
inline ARCHIVE_DATA_MODE end( ARCHIVE_DATA_MODE mode )
{
	auto e = ARCHIVE_DATA_MODE::END;
	return ++e;
}
}
#endif // DEFINES_H
