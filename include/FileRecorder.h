#ifndef FILERECORDER_H
#define FILERECORDER_H
#include "Defines.h"
#include "LogRecorder.h"
#include <filesystem>
#include <gtest/gtest_prod.h>
#include <memory>
namespace X
{
class PrivateFileRecorder;
/**
 * @brief The FileRecorder class is first and basic implementation of recorder. In it construction user must set behavior with old data and recorder name. Settings will be override by configuration file. First behavior is archive old data library try to compress old data. In second option only two newest files will be saved oldest will be override. Second arguments will be used to create file on storage to save logs. Old one, if it exists will be replaced by name.log.1
 */
class FileRecorder : public LogRecorder
{
	FRIEND_TEST( fileRecorder, save );

public:
	/**
	 * @brief Constructs FileRecorder. Constructs a new FileRecord object. Passed argument initialize parent class, and also private implementation pointer in pimpl technique.
	 * @param name
	 */
	explicit FileRecorder( const std::string& name = "file.log" );
	// LogRecorder interface
	/**
	 * @brief This is ovrrided function to save msg in file.
	 * @param type
	 * @param msg
	*/
	void save( LOG_TYPE type, const std::string& msg ) override;
	/**
	 * @brief Getter to member "logPath". Return path to log file.
	 * @return
	 */
	std::string logPath() const;
	/**
	 * @brief Setter to member "logPath". Set path to log file.
	 * @param logPath
	 */
	void setLogPath( const std::string& logPath );


	/**
	 * @brief Getter to member "enableConsoleColorsTags". If this property is enabled, to all log will be added console color tag.
	 * @return
	 */
	bool enableConsoleColorsTags() const;
	/**
	 * @brief Setter to member "enableConsoleColorsTags".
	 * @param enableConsoleColorsTags
	 */
	void setEnableConsoleColorsTags( bool enableConsoleColorsTags );
	/**
	 * @brief Overloaded setter for "enableConsoleColorsTags" member.
	 * @param value
	 */
	void setEnableConsoleColorsTags( const std::string& value );

private:
	std::shared_ptr< PrivateFileRecorder > _p;
};
} // namespace X
#endif // FILERECORDER_H
